import { clientsBase, characters, user1, satoshiInfo, books, employeeData, array, } from "./data.js";

// Task 1
const newDataBase = [...new Set([...clientsBase.clients1, ...clientsBase.clients2]),];

// Task 2
const shortInfo = characters.map(({ gender, status, ...rest }) => rest);

// Task 3
const { name, years, isAdmin = false } = user1;

// Task 4
const fullProfile = { ...satoshiInfo.satoshi2018, ...satoshiInfo.satoshi2019, ...satoshiInfo.satoshi2020 };

// Task 5
const newBooksCollection = [books.bookToAdd, ...books.books];

// Task 6
const newEmployee = { ...employeeData.employee, ...employeeData.newEmployeeProps };

// Task 7
const [value, showValue] = array;

console.log("Task 1", newDataBase);
console.log("Task 2", shortInfo);
console.log("Task 3", name, years, isAdmin);
console.log("Task 4", fullProfile);
console.log("Task 5", newBooksCollection);
console.log("Task 6", newEmployee);
console.log("Task 7", value, showValue());
